<?php

namespace Zotlabs\Module;

use App;
use Zotlabs\Lib\Apps;
use Zotlabs\Web\Controller;

function rs($key,$len)
{
        return (substr(trim(filter_var($_POST[$key],FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH)),0,$len));
}

function ers($val,$len)
{
        return (substr(trim(filter_var($val,FILTER_SANITIZE_STRING,FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH)),0,$len));
}

class Fedfollow extends Controller {

	function get()
	{

		$convert_link = true;

		$ca = ers($_GET['ca'],32);
		if ($ca=='') $ca = rs('ca',32);

		$cz = '';
		if ($ca=='')
		{
			$cz = ers($_GET['cz'],255);
			if ($cz=='') $cz = rs('cz',255);
			$convert_link = false;
		}

		$cn = ers($_GET['cn'],64);

		if (($ca!='')||($cz!=''))
		{
			$usr = rs('usr',128);
			if ($usr!='')
			{
				//do connect

				if (strstr($usr,'%')) exit();

				$err=array();
				if (!strstr($usr,'@'))
				{
					$err[]='Please enter your federated username';
				} else {
					$x=explode('@',$usr);
					$domain = array_pop($x);
					if (!strstr($domain,'.'))
					{
						$err[]='Please enter your federated username';
					}
				}

				$subscribe_url = '';

				if (count($err)<1)
				{
					$f=file_get_contents('https://'.$domain.'/.well-known/webfinger?resource=acct:'.$usr);
					$x=json_decode($f,true);
					if (!is_array($x))
					{
						$err[]='I could not locate that account. Please verify your federated username.';
					} else {
						if (array_key_exists('links',$x) && (count($x['links'])>0))
						{
							foreach ($x['links'] as $l=>$m)
							{
								if ($m['rel']=='http://ostatus.org/schema/1.0/subscribe')
								{
									$subscribe_url = $m['template'];
									if ($convert_link)
									{
										$subscribe_url = str_replace('{uri}',
											urlencode(z_root().'/@'.$ca),
											$subscribe_url);
									} else {
										$subscribe_url = str_replace('{uri}',
											urlencode($cz),
											$subscribe_url);
									}
										
								}
							}
						}
					}
				}

				if ($subscribe_url=='')
				{
					$err[]='I could not locate that account. Please verify your federated username.';
				}

				if (count($err)>0)
				{
					$o = '<h3>OOps</h3><p>We encountered an error with your input.'.join('<br>',$err).'</p>
<p>Enter your federated user@domain.tld account and we will redirect you to your site.</p>
<form method="post" action="/fedfollow">
<input type="hidden" name="ca" value="'.htmlentities($ca).'">
<input type="hidden" name="cz" value="'.htmlentities($cz).'">
<input type="email" name="usr" value="'.htmlentities($usr).'" size="20" required="required" placeholder="user@domain.tld">
<button type="submit" class="btn btn-sm btn-primary">Go</button>
</form>
';
				} else {
					Header("Location: ".$subscribe_url);
					exit();
				}

			} else {
				$o = '<h1>Connect With '.htmlentities($cn).'</h1>
<p>Enter your federated user@domain.tld account and we will redirect you to your site.</p>
<form method="post" action="/fedfollow">
<input type="hidden" name="ca" value="'.htmlentities($ca).'">
<input type="hidden" name="cz" value="'.htmlentities($cz).'">
<input type="email" name="usr" value="" size="20" required="required" placeholder="user@domain.tld">
<button type="submit" class="btn btn-sm btn-primary">Go</button>
</form>
';
			}
		} else {
			$o = '<h1 class="text-danger">Internal Error</h1>';
		}
		return $o;
	}
	
}
