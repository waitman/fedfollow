<?php


/**
 * Name: Fedfollow
 * Description: Add Federated Follow button on profile
 * Version: 1.0
 * Author: Waitman Gobble
 * Maintainer: Waitman Gobble <waitman@waitman.net>
 */

use Zotlabs\Lib\Apps;
use Zotlabs\Extend\Hook;
use Zotlabs\Extend\Route;

function fedfollow_load() {
	Hook::register('profile_sidebar', 'addon/fedfollow/fedfollow.php', 'fedfollow_connect_button');
	Hook::register('dropdown_extras', 'addon/fedfollow/fedfollow.php', 'fedfollow_like');
	Route::register('addon/fedfollow/Mod_Fedfollow.php','fedfollow');
}

function fedfollow_unload() {
	Hook::unregister('profile_sidebar', 'addon/fedfollow/fedfollow.php', 'fedfollow_connect_button');
	Hook::unregister('dropdown_extras', 'addon/fedfollow/fedfollow.php', 'fedfollow_like');
	Route::unregister('addon/fedfollow/Mod_Fedfollow.php','fedfollow');
}

function fedfollow_connect_button(&$b) {

	if(! App::$profile_uid)
		return;

	if(! Apps::addon_app_installed(App::$profile_uid,'fedfollow'))
		return;

	//if they are already logged into Zap then split.
	if (local_channel())
		return;

	//check if there is already a follow button (they are logged into Zap)
	if (!strstr($b['entry'],'connect-btn-wrapper'))
	{

		$follow_button = '
<div class="connect-btn-wrapper">
<a href="/fedfollow?ca='.urlencode($b['profile']['channel_address']).'&amp;cn='.urlencode($b['profile']['channel_name']).'" 
	class="btn btn-block btn-success btn-sm">
<i class="fa fa-plus"></i> Connect
</a>
</div>
';
		//put it above profile name
		$b['entry'] = str_replace('<div class="fn p-name">',
			$follow_button.'<div class="fn p-name">',
			$b['entry']);


	}
}

function fedfollow_like(&$b) {

        if(! App::$profile_uid)
                return;

        if(! Apps::addon_app_installed(App::$profile_uid,'fedfollow'))
                return;

        //if they are already logged into Zap then split.
        if (local_channel())
                return;

	$lnk = $b['item']['mid'];
	$a =  '<a class="dropdown-item" href="/fedfollow?cz='.urlencode($lnk).'&amp;cn=Item">Like/Repost</a>' ;
	$b['dropdown_extras']=$a;

}

